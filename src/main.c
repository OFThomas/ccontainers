/**
 * @author      : oli (oli@pop-os)
 * @file        : main
 * @created     : Sunday Jul 09, 2023 21:59:38 BST
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Maybe first is c implementation of std::vector? 
// Then want a std::map, like a hash table?
// Probably a good start 

// we'll use the ol prefix 
struct olVector {
  int *data;
  int size;
  int capacity;
};

struct olVector *olVectorNew()
{
  struct olVector *v = malloc(sizeof(struct olVector));
  if (v == NULL) {
    exit(1);
  }
  v->data = malloc(sizeof(*v->data) * 10);
  if (v->data == NULL) {
    exit(1);
  }
  v->size = 0;
  v->capacity = 8;
  return v;
}

void olVectorFree(struct olVector *v)
{
  free(v->data);
  free(v);
}

void olVectorCheckRealloc(struct olVector *v)
{
  if (v->size == v->capacity) {
    v->capacity *= 2;
    v->data = realloc(v->data, sizeof(*v->data) * v->capacity);
    if (v->data == NULL) {
      exit(1);
    }
  }
}

void olVectorResize(struct olVector *v, int size)
{
  // Check size makes sense
  if (size < 0) {
    exit(1);
  }

  v->size = size;
  olVectorCheckRealloc(v);
}

void olVectorPush(struct olVector *v, int val)
{
  olVectorCheckRealloc(v);
  v->data[v->size++] = val;
}

void olVectorPop(struct olVector *v)
{
  if (v->size == 0) {
    return;
  }
  v->size--;
}

int olVectorFront(struct olVector *v)
{
  return v->data[0];
}

int olVectorBack(struct olVector *v) 
{
  return v->data[v->size-1];
}

void olVectorInsert(struct olVector *v, int val, int idx)
{
  if(idx < 0 || idx > v->size) {
    exit(1);
    return;
  }

  if (idx == v->size) {
    olVectorPush(v, val);
    return;
  }
  else {
    // we need to move all the elements from idx to idx + 1
    // but going BACKWARDS
    // copy the last element (and increase the size by 1)
    olVectorPush(v, olVectorBack(v));
    // last - 1 to idx + 1 move 1 forward
    for (int i = v->size - 1; i > idx; i--) {
      v->data[i] = v->data[i-1];
    }
    // Then insert out new value
    v->data[idx] = val;
  }
}

void olVectorPrint(struct olVector *v) 
{
  printf("Size %d, Capacity %d\n[", v->size, v->capacity);
  for (int i = 0; i < v->size - 1; i++) {
    printf("%d, ", v->data[i]);
  }
  if (v->size > 0) {
    printf("%d", olVectorBack(v));
  }
  printf("]\n");
}


// So maps now?

// Our string hash function is the djb2 
// http://www.cse.yorku.ca/~oz/hash.html
// https://stackoverflow.com/questions/7666509/hash-function-for-string

// probably need to use long or something
// need to check the spec for int, think it's only >= 16bit signed
size_t olStrHash(const char* str)
{
  size_t hash = 5381;
  int c;
  while ((c = *str++)) {
    hash = ((hash << 5) + hash) + c;
  }
  return hash;
}

// A map needs keys and values 
// The simplest map is an array of key, value pairs
typedef size_t Key_t;
typedef char* Value_t;

#define OLMAPMAXCAPACITY 100

struct olMap {
  size_t size;
  size_t capacity;
  Key_t keys[OLMAPMAXCAPACITY];
  Value_t values[OLMAPMAXCAPACITY];
};

struct olMap *olMapNew()
{
  struct olMap *map = malloc(sizeof(struct olMap));
  if (map == NULL) {
    exit(1);
  }
  map->size = 0;
  map->capacity = OLMAPMAXCAPACITY;
  return map;
}

void olMapFree(struct olMap *map) 
{
  free(map);
}

// There are maps which is an array of key value structs 
// there are flat maps which is an array of keys
// an array of values 
// insertion is 
void olMapInsert(struct olMap *map, const char* key, Value_t value)
{
  if (map->size >= map->capacity) {
    exit(1);
  }

  map->keys[map->size] = olStrHash(key);
  map->values[map->size] = value;
  map->size++;

}

int olMapFind(struct olMap *map, const char* key) 
{
  for (size_t i = 0; i < map->size; i++) {
    if (map->keys[i] == olStrHash(key)) {
      return i;
    }
  }
  printf("Err %s not found\n", key);
  // abort();
  exit(1);
  return -1;
}

void olMapPrint(struct olMap *map)
{
  for (size_t i = 0; i < map->size; i++) {
    printf("idx %lu, key %lu, val %s\n", i, map->keys[i], map->values[i]);
  }
}

int vectorMain(void) 
{
  struct olVector *v = olVectorNew();
  olVectorPrint(v);

  for(int i = 0; i < 10; i++) {
    olVectorPush(v, i);
    olVectorPrint(v);
  }

  olVectorInsert(v, -1, 5);

  olVectorPrint(v);

  olVectorPop(v);
  olVectorPrint(v);

  olVectorResize(v, 16);
  olVectorPrint(v);

  olVectorResize(v, 6);
  olVectorPrint(v);

  return 0;
}

int mapMain(void)
{
  struct olMap *map = olMapNew();
  olMapPrint(map);

  for (int i = 0; i < 10; i++) {
    char myString[10]; 
    sprintf(myString, "%c", 'a'+ i);
    olMapInsert(map, myString, 0);
  }

  olMapPrint(map);

  printf("Capacity %lu, Size %lu\n", map->capacity, map->size);
  for (int i = 0; i < map->size; i++) {
    char myString[10]; 
    sprintf(myString, "%c", 'a'+ i);
    int pos = olMapFind(map, myString);
    printf("Found %d at %d\n", i+10, pos);
  }

  // olMapFind(map, -1);

  olMapFree(map);
  return 0;
}

int main()
{
  vectorMain();

  mapMain();

  // Map 2 
  printf("Map number 2\n");
  struct olMap* map = olMapNew();
  const char* str1 = "a";
  const char* str2 = "b";
  const char* lastStr = "z";

  // please forgive me
  const char* keys[3] = {str1, str2, lastStr};

  olMapInsert(map, str1, "no");
  olMapInsert(map, str2, "pls");

  olMapInsert(map, lastStr, "Segfault?");
  olMapPrint(map);

  for (int i = 0; i < 3; i++) {
    olMapFind(map, keys[i]);
  }






  return 0;
}

